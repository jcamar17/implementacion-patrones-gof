// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBR_KwhqDHwuX8C0uwSrdRABtZ-3Li_e9A',
    authDomain: 'juan-ibero.firebaseapp.com',
    projectId: 'juan-ibero',
    storageBucket: 'juan-ibero.appspot.com',
    messagingSenderId: '819261972885',
    appId: '1:819261972885:web:208b8b492372b37533969c',
    measurementId: 'G-C578GN8RG1'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
