import { Injectable } from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {MessageModel} from '../../models/message.model';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  // Inyección de dependencias
  constructor(
    private store: AngularFirestore
  ) { }

  public createMessage(data: MessageModel): any{
    return this.store.collection('msg').add(data);
  }

  public getMessages(): Observable<any>{
    return this.store.collection('msg').snapshotChanges();
  }

  public getMessage(documentId: string): Observable<any>{
    return this.store.collection('msg').doc(documentId).snapshotChanges();
  }

  public notifyMessages(): Observable<any>{
    return this.store.collection('msg').snapshotChanges().pipe(
      map(data => data.length)
    );
  }
}
