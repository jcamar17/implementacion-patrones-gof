import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {MessageModel} from '../../models/message.model';
import {FirebaseService} from '../../services/firebase/firebase.service';
import {MatTableDataSource} from '@angular/material/table';

// Se hace uso del patrón decorator para impementar funcionalidades de la clase factoryDecorator
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  displayedColumns: string[] = ['title', 'content', 'publishDate'];
  messages: MessageModel[] = [];
  dataSource: any;

  // Se implementa el principio de inyección de dependencias a lo largo del desarrollo.
  constructor(
    private router: Router,
    private firebase: FirebaseService
  ) {}
  // Se utiliza el patrón observer para mostrar cambios en la lista de publicaciónes
  ngOnInit(): void {
    this.firebase.getMessages().subscribe((messagesSnapshot) => {
      messagesSnapshot.forEach( (msg: any) => {
        this.messages.push({
          title: msg.payload.doc.data().title,
          content: msg.payload.doc.data().content,
          publishDate: msg.payload.doc.data().publishDate.toDate(),
        });
        this.dataSource = new MatTableDataSource(this.messages);
      });
    });
  }

  newMessage(): void {
    this.router.navigate(['messages/new']);
  }

}
