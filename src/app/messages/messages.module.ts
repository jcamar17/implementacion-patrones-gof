import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MessagesRoutingModule } from './messages-routing.module';
import { ListComponent } from './list/list.component';
import { NewComponent } from './new/new.component';
import {SharedModule} from '../shared/shared.module';


@NgModule({
  declarations: [ListComponent, NewComponent],
  exports: [
    ListComponent
  ],
  imports: [
    CommonModule,
    MessagesRoutingModule,
    SharedModule
  ]
})
export class MessagesModule { }
