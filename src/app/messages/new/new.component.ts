import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {FirebaseService} from '../../services/firebase/firebase.service';
import {MessageModel} from '../../models/message.model';
import firebase from 'firebase';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Router} from '@angular/router';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.sass']
})
export class NewComponent implements OnInit {

  form = new FormGroup({
    title: new FormControl('', Validators.required),
    content: new FormControl('', Validators.required)
  });

  constructor(
    private store: FirebaseService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {}

  ngOnInit(): void{
  }

  saveMessage(): void{
    if (this.form.valid){
      const data: MessageModel = {
        title: this.form.value.title,
        content: this.form.value.content,
        publishDate: firebase.firestore.FieldValue.serverTimestamp()
      };
      this.store.createMessage(data).then(() => {
        this.showSnack('Publicación generada exitosamente');
        this.form.setValue({
          title: '',
          content: ''
        });
      });
    }
  }

  showSnack(message: string): void{
    this.snackBar.open(message, 'cerrar', {
      duration: 3000
    });
  }

  showMessages(): void{
    this.router.navigate(['/messages']);
  }

}
