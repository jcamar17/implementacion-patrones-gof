import { Component, OnInit } from '@angular/core';
import {MessageModel} from '../../models/message.model';
import {FirebaseService} from '../../services/firebase/firebase.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
  messages: MessageModel[] = [];

  constructor(
    private store: FirebaseService
  ) { }

  ngOnInit(): void {
    this.store.getMessages().subscribe( snapshotMessages => {
      this.messages = [];
      snapshotMessages.forEach( (msg: any) => {
        this.messages.push({
          title: msg.payload.doc.data().title,
          content: msg.payload.doc.data().content,
          publishDate: msg.payload.doc.data().publishDate.toDate(),
        });
      });
    });
  }

}
