export interface MessageModel {
  title: string;
  content: string;
  publishDate: any;
}

